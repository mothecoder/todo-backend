package com.huneidi.apps.todoapp;

import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoItem>  getTodoItems(){
        return todoRepository.findAll();
    }

    public TodoItem findByTitle(String title){

        Optional<TodoItem> byName = todoRepository.findByTitle(title);
        if (!byName.isPresent()) {
            throw new TodoItemNotFoundException();
        }
        return byName.get();
    }
    public TodoItem saveItem(TodoItem todoItem){
        return todoRepository.save(todoItem);
    }

    public void deleteItem(String id) {
         todoRepository.deleteById(id);
    }

    public List<TodoItem> findByUserId(String userId){
       return todoRepository.findAllByUserId(userId);
    }
}
