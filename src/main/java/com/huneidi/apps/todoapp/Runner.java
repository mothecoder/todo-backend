package com.huneidi.apps.todoapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Profile("local")
public class Runner {
    @Value("${application.welcome.message}")
    private String welcomeMessage;


    @EventListener(ApplicationReadyEvent.class)
    public void test(){
        System.out.println(welcomeMessage);
    }
}
