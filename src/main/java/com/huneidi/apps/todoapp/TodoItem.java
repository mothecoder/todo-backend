package com.huneidi.apps.todoapp;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection ="todo-items")
public class TodoItem {

    @Id
    private String id;
    private String userId;
    private String description;
    private String title;
    public TodoItem(String userId,  String title, String description) {
        this.title = title;
        this.description = description;
        this.userId = userId;
    }

    public TodoItem() {
    }
}
