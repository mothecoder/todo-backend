package com.huneidi.apps.todoapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class TodoController {

    private final TodoService todoService;


    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todo/get-items")
    public ResponseEntity<List<TodoItem>> getTodoItems(){
        List<TodoItem> todoItems = todoService.getTodoItems();
        return new ResponseEntity<>(todoItems, HttpStatus.OK);
    }

    @PostMapping("/todo/save-item")
    public ResponseEntity<Object> saveItem(@RequestBody TodoItem todoItem){
        todoService.saveItem(todoItem);
        return ResponseEntity.noContent().build();
    }

//    @PostMapping("/todo/update-item")
//    public ResponseEntity<String> updateItem(@RequestBody TodoItem todoItem){
//        todoService.saveItem(todoItem);
//        return new ResponseEntity<>("Item saved successfully!", HttpStatus.OK);
//    }


    @GetMapping("/todo/find-by-title/")
    public ResponseEntity<TodoItem> findByTitle(@RequestParam("title") String title){
        TodoItem item = todoService.findByTitle(title);
        return new ResponseEntity<>(item, HttpStatus.OK);
    }
    @GetMapping("/todo/find-by-user-id/")
    public ResponseEntity<List<TodoItem>> findByUserId(@RequestParam("userId") String userId){
        List<TodoItem> items = todoService.findByUserId(userId);
        return new ResponseEntity<>(items, HttpStatus.OK);
    }
    @GetMapping("/todo/delete-item/")
    public ResponseEntity<Void> deleteItem(@RequestParam("id") String id){
        todoService.deleteItem(id);
        return new ResponseEntity<>( HttpStatus.OK);
    }
}
