package com.huneidi.apps.todoapp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class TodoExceptionAdvice {

    @ExceptionHandler(value = TodoItemNotFoundException.class)
    public ResponseEntity<Object> itemNotFoundHandler(TodoItemNotFoundException ex){
        return new ResponseEntity<>("The Todo Item you searched for was not found", HttpStatus.NOT_FOUND);
    }
}
