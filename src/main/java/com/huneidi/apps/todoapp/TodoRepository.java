package com.huneidi.apps.todoapp;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TodoRepository extends MongoRepository<TodoItem, String> {

    public Optional<TodoItem> findByTitle(String title);

    @Override
    Optional<TodoItem> findById(String s);

    List<TodoItem> findAllByUserId(String userId);

    public Optional<TodoItem> findByDescription(String description);
}
